#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <Windows.h>
/*
�������� ��������-��������, ��� ������ �� ����������� ����������� �
�������� ���� �� ����� � ������ ����������� ����� � ����. ���� ����� � ����
����� ����������� �������� �������� �������� ���������.*/
int main() {
	char m[128], codes[128][6];
	strcpy(codes['A'], ".-");
	strcpy(codes['B'], "-...");
	strcpy(codes['C'], "-.-.");
	strcpy(codes['D'], "-..");
	strcpy(codes['E'], ".");
	strcpy(codes['F'], "..-.");
	strcpy(codes['G'], "--.");
	strcpy(codes['H'], "....");
	strcpy(codes['I'], "..");
	strcpy(codes['J'], ".---");
	strcpy(codes['K'], "-.-");
	strcpy(codes['L'], ".-..");
	strcpy(codes['M'], "--");
	strcpy(codes['N'], "-.");
	strcpy(codes['O'], "---");
	strcpy(codes['P'], ".--.");
	strcpy(codes['Q'], "--.-");
	strcpy(codes['R'], ".-.");
	strcpy(codes['S'], "...");
	strcpy(codes['T'], "-");
	strcpy(codes['U'], "..-");
	strcpy(codes['V'], "...-");
	strcpy(codes['W'], ".--");
	strcpy(codes['X'], "-..-");
	strcpy(codes['Y'], "-.--");
	strcpy(codes['Z'], "--..");
	strcpy(codes['0'], "-----");
	strcpy(codes['1'], ".----");
	strcpy(codes['2'], "..---");
	strcpy(codes['3'], "...--");
	strcpy(codes['4'], "....-");
	strcpy(codes['5'], ".....");
	strcpy(codes['6'], "-....");
	strcpy(codes['7'], "--...");
	strcpy(codes['8'], "---..");
	strcpy(codes['9'], "----.");
	strcpy(codes[' '], " ");
	printf("Enter a string: ");
	gets_s(m);
	int i = 0;
	while (m[i] != '\0') {
		m[i] = toupper(m[i]);
		char* code = codes[m[i]];
		int j = 0;
		while (code[j] != '\0') {
			if (code[j] == ' ') {
				printf(" ", code[j]);
			}
			if (code[j] == '.') {
				printf(".", code[j]);
				Beep(1000, 100);
			}
			if (code[j] == '-') {
				printf("-");
				Beep(1000, 200);
			}
			Sleep(30);
			j++;
		}
		printf(" ", code[j]);
		Sleep(300);
		i++;
	}
	return 0;
}