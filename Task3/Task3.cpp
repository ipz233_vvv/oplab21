#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define SIZE 256
/*
�������� 3
� ��������� ��������� ��������� �����. ��������� ��������, ��
����������� ���������:
�) �������� ������� ����� ���, �� ������� �� �������� ������;
�) �������� �� ����� ������� ������������ �������; 
�) ������� �� �����, �� ����� ������� �����.
*/
char** copy(char** m, int size) {
	char** d= (char**)malloc(size*sizeof(char*));
	for (int i = 0; i < size; ++i) {
		*(d+i) = (char*)malloc((strlen(*(m+i)) + 1)*sizeof(char*));
		strcpy(*(d+i), *(m+i));
	}
	return d;
}
int uniqueWords(char** words, int size) {
	int d = size;
	char** wc = copy(words, size);
	for (int i = 0; i < size - 1; ++i) {
		for (int j = i + 1; j < size; ++j) {
			if (*(wc + i) != NULL && *(wc + j) != NULL)
				if (strcmp(*(wc + i), *(wc + j)) == 0) {
					free(*(wc + j));
					*(wc + j) = NULL;
				}
		}
	}
	for (int i = 0; i < size; i++) {
		if (*(wc + i) == NULL)
			d--;
		else
			free(*(wc + i));
	}
	free(wc);
	return d;
}
bool hasDouble(char* word) {
	int i = 1;
	while (word[i] != '\0') {
		if (word[i] == word[i - 1])
			return true;
		i++;
	}
	return false;
}
char** rmWords(char **words,int *size) {
	char** new_words = (char**) malloc((* size+1) * sizeof(char*));
	int nn=0;
	for (int i = 0; i < *size; i++) {
		if (!hasDouble(words[i]))
		{
			new_words[nn] = (char*)malloc((strlen(words[i])+1) * sizeof(char));
			strcpy(new_words[nn], words[i]);
			nn++;
		}
		free(words[i]);
	}
	free(words);
	*size = nn;
	return new_words;
}
int main() {
	char m[SIZE];
	printf("Enter a string: ");
	gets_s(m);
	char** words=(char **)malloc(SIZE/2*sizeof(char*));
	int wn = 0;
	int s = 0;
	char* word = strtok(m, " ");
	while (word != NULL) {
		int l = strlen(word);
		*(words + wn) = (char*)malloc((l+1) * sizeof(char));
		strcpy(*(words + wn),word);
		s += strlen(word);
		wn++;
		word = strtok(NULL, " ");
	}
	printf("Unique words count: %d\n", uniqueWords(words,wn));
	printf("Characters used: %d\n", s);
	words = rmWords(words, &wn);
	printf("String without words with double characters:\n", s);
	for (int i = 0; i < wn; i++)
	{
		printf("%s ", (*(words + i)));
	}
	for (int i = 0; i < wn; i++) {
		free(words[i]);
	}
	free(words);
	return 0;
}